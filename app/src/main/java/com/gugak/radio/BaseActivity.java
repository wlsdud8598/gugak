package com.gugak.radio;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;
import com.gugak.radio.base.BaseFragment;
import com.gugak.radio.base.DummyActivity;
import com.gugak.radio.base.DummyPopActivity;
import com.gugak.radio.component.CDialog;
import com.gugak.radio.main.MainFragment;
import com.gugak.radio.network.tr.ApiData;
import com.gugak.radio.network.tr.BaseData;
import com.gugak.radio.network.tr.CConnAsyncTask;
import com.gugak.radio.network.tr.NetworkUtil;
import com.gugak.radio.network.tr.data.Tr_KA001;
import com.gugak.radio.util.CDateUtil;
import com.gugak.radio.util.Logger;
import com.gugak.radio.util.SharedPref;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

public class BaseActivity extends AppCompatActivity implements  NavigationView.OnNavigationItemSelectedListener{
    private final String TAG = BaseActivity.class.getSimpleName();

    public static final String FRAGMENT_NAME = "fragment_name";
    public static final String FRAGMENT_BUNDLE = "fragment_bundle";

    public static final int ACTIVITY_REQ_CODE = 8888;
    public static final int BACK_DATA_CODE = 9999;
    public static final String ACTIVITY_BACK_DATA = "activity_back_data";

    private DrawerLayout mdrawer;
    private ImageButton menu;
    protected LinearLayout mProgressView;

    View.OnClickListener mClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            Bundle bundle = new Bundle();
            switch (v.getId()) {

                case R.id.menu_btn:
                    mdrawer.openDrawer(GravityCompat.START);
                    break;

                case R.id.radio_btn:
                    break;

                case R.id.tv_btn:
                    break;
            }

        }
    };

    public BaseFragment getFragment() {
        Intent intent = getIntent();
        BaseFragment fragment = null;
        try {
            String className = intent.getStringExtra(FRAGMENT_NAME);
            Class<?> cl = Class.forName(className);
            Constructor<?> co = cl.getConstructor();
            fragment = (BaseFragment) co.newInstance();
        } catch (Exception e) {
            Log.e(TAG, "getFragment", e);
        }

        return fragment;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        SharedPref.getInstance().initContext(BaseActivity.this);
        mProgressView = findViewById(R.id.main_progress);
        mdrawer = findViewById(R.id.drawer_layout);
        mdrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        menu = findViewById(R.id.menu_btn);
        menu.setOnClickListener(mClickListener);
    }

    private String hideProgressTag;
    private int progressCnt = 0;

    public void showProgress() {
        String tag = CDateUtil.getForamtyyyy_MM_dd_HH_mm_ss(new Date());
        mProgressView.setVisibility(View.VISIBLE);
        mProgressView.setTag(tag);

        hideProgressTag = TextUtils.isEmpty(hideProgressTag) ? tag : hideProgressTag;

        if (progressCnt < 0)
            progressCnt = 0;

        progressCnt++;

        Logger.i(TAG, "showProgress progressCnt="+progressCnt);
    }

    public void hideProgress() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (hideProgressTag == null) {
                    mProgressView.setVisibility(View.GONE);
                    return;
                }

                progressCnt--;
                Logger.i(TAG, "HideProgress progressCnt="+progressCnt);

                if (progressCnt <= 0)
                    mProgressView.setVisibility(View.GONE);

            }
        }, 200);

//        mCommonActionBar.showBlockLayout(false);
    }


    public void hideProgressForce() {
        progressCnt = 0;
        hideProgress();
    }

    /**
     * 최상위 플래그먼트인지 여부(로그인, 또는 메인화면)
     * @param fragment
     * @return
     */
    private boolean isMainFragment(Fragment fragment) {
        boolean isMain = (fragment instanceof MainFragment);
        return isMain;
    }

    public void replaceFragment(Fragment fragment) {
        replaceFragment(fragment, null);
    }

    public void replaceFragment(Fragment fragment, Bundle bundle) {
        boolean isBackStack = isMainFragment(fragment) ? false : true;
        replaceFragment(fragment, isBackStack, true, bundle);
    }

    public void movePageActivity(Fragment fragment, Bundle bundle) {
        DummyActivity.startActivityForResult(BaseActivity.this, ACTIVITY_REQ_CODE, fragment.getClass(),  bundle, true);
    }

    public void movePageNohistoryActivity(Fragment fragment, Bundle bundle) {
        DummyActivity.startNohistoryActivity(BaseActivity.this, fragment,  bundle);
    }

    public void movePageNohistoryActivity(Fragment fragment) {
        movePageNohistoryActivity(fragment, null);
    }

    public void movePagePopActivity(Fragment fragment, Bundle bundle) {
        DummyPopActivity.startActivityForResult(BaseActivity.this, ACTIVITY_REQ_CODE, fragment.getClass(),  bundle, true);
    }

    public void movePageNoAnimActivity(Fragment fragment, Bundle bundle,boolean anim) {
        DummyActivity.startActivity(BaseActivity.this,  fragment,  bundle, anim);
    }

    public void movePageActivity(Fragment fragment) {
        movePageActivity(fragment, null);
    }

    public void replaceFragment(final Fragment fragment, final boolean isAddBackStack, boolean isAnim, Bundle bundle) {
        hideProgressForce();

        BaseFragment.newInstance(this);
        // 메인 플래그먼트면 history stack 모두 제거
        if (isMainFragment(fragment))
            removeAllFragment();

        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (isAnim && isMainFragment(fragment) == false)
            transaction.setCustomAnimations(R.anim.fragment_enter, R.anim.fragment_exit, R.anim.fragment_pop_enter, R.anim.fragment_pop_exit);
        if (bundle != null)
            fragment.setArguments(bundle);


        transaction.replace(R.id.content_layout, fragment, fragment.getClass().getSimpleName());
        if (!isFinishing() && !isDestroyed()) {

            if (isAddBackStack)
                transaction.addToBackStack(null);

            transaction.commit();
        } else {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (isAddBackStack)
                        transaction.addToBackStack(null);

                    transaction.commitAllowingStateLoss();
                }
            }, 100);
        }

        printFragmentLog();
    }

    public BaseFragment getVisibleFragment() {
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment != null && fragment.isVisible()) {
                return (BaseFragment) fragment;
            }
        }
        return null;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    public void removeAllFragment() {
        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStackImmediate();
    }


    private void printFragmentLog() {
        if (getSupportFragmentManager().getFragments() != null) {
            Logger.i(TAG, "replaceFragment.size=" + getSupportFragmentManager().getFragments().size());

            for (Fragment fg : getSupportFragmentManager().getFragments()) {
                if (fg != null)
                    Logger.i(TAG, "replaceFragment.name=" + fg.toString());
            }
        }
    }

    public void finishStep() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if(drawer.isDrawerOpen(GravityCompat.END)){
            drawer.closeDrawer(GravityCompat.END);
        }else {
            if (mIsFinishing) {
                mIsFinishing = false;
                super.onBackPressed();
            } else {
                if (getClass().getName().equals(MainActivity.class.getName())) {
                    Toast.makeText(getApplicationContext(), getString(R.string.message_finish_message), Toast.LENGTH_SHORT).show();
                    mIsFinishing = true;
                    mMasterHandler.sendEmptyMessageDelayed(HANDLE_FINISHING_EXPIRE, 3000L);
                } else {
                    super.onBackPressed();
                }
            }
        }
    }

    protected boolean mIsFinishing = false;
    public final int HANDLE_FINISHING_EXPIRE = -999;
    protected Handler mMasterHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case HANDLE_FINISHING_EXPIRE:
                    mIsFinishing = false;
                    break;
            }
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        Logger.i(TAG, TAG + ".onResume()");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Logger.i(TAG, TAG + ".onActivityResult"+requestCode);

        if(resultCode == BACK_DATA_CODE) {
            // BackData 처리
            if (data != null) {
                Bundle backData = data.getBundleExtra(ACTIVITY_BACK_DATA);
                Log.i(TAG, "backData="+backData);
                if (getVisibleFragment() != null) {
                    getVisibleFragment().resultBackData(backData);
                    getVisibleFragment().onActivityResult(requestCode, resultCode, data);
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (getVisibleFragment() != null) {
            getVisibleFragment().onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BaseFragment baseFragment = (BaseFragment) getVisibleFragment();
        if (baseFragment != null) {
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if(drawer.isDrawerOpen(GravityCompat.END)){
            drawer.closeDrawer(GravityCompat.END);
        }else if (getVisibleFragment() instanceof BaseFragment) {
            int backCnt = getSupportFragmentManager().getBackStackEntryCount();
            Logger.i(TAG, "onBackPressed.backCnt=" + getSupportFragmentManager().getBackStackEntryCount() + " ,getClass()=" + getClass());
            if (getVisibleFragment() instanceof BaseFragment) {
                BaseFragment baseFragment = (BaseFragment)getVisibleFragment();
                baseFragment.onBackPressed();
            }
        }else {
            super.onBackPressed();
        }
    }

    public void superBackPressed() {
        super.onBackPressed();
    }

    private DrawerLayout.DrawerListener mDrawListener = new DrawerLayout.DrawerListener() {
        @Override
        public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

        }

        @Override
        public void onDrawerOpened(@NonNull View drawerView) {
        }

        @Override
        public void onDrawerClosed(@NonNull View drawerView) {
        }

        @Override
        public void onDrawerStateChanged(int newState) {

        }
    };

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_radio) {
            Log.i(TAG, "onNavigationItemSelected: PRINTED!");
        } else if (id == R.id.nav_replay) {
            Log.i(TAG, "onNavigationItemSelected: ");
        } else if (id == R.id.nav_tv) {
            Log.i(TAG, "onNavigationItemSelected: PRINTED!");

        } else if (id == R.id.nav_settings) {
            Log.i(TAG, "onNavigationItemSelected: PRINTED!");

        } else if (id == R.id.nav_terms) {
            Log.i(TAG, "onNavigationItemSelected: PRINTED!");

        } else if (id == R.id.nav_hompage) {
            Log.i(TAG, "onNavigationItemSelected: PRINTED!");

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.END);
        return true;
    }

    /**
     * 공통 전문통신
     * 현재는 키워드에서만 사용 되고 있음
     * Fragment 에서는 getData 사용 중
     * @param cls
     * @param obj
     * @param step
     */
    public void getTrData(final BaseData cls, final Object obj, final ApiData.IStep step) {
//    public void getTrData(final BaseData cls, final ApiData.IStep step) {

        if (NetworkUtil.getConnectivityStatus(BaseActivity.this) == false) {
            CDialog.showDlg(this, getString(R.string.text_network_error), new CDialog.DismissListener() {
                @Override
                public void onDissmiss() {
                    finish();
                }
            });
            return;
        }

        CConnAsyncTask.CConnectorListener queryListener = new CConnAsyncTask.CConnectorListener() {
            @Override
            public Object run() {
                ApiData data = new ApiData();
                return data.getData(cls, obj);
            }

            @Override
            public void view(CConnAsyncTask.CQueryResult result) {
                Logger.i(TAG, "result.result=" + result.result + ", result.data=" + result.data);
                if (result.result == CConnAsyncTask.CQueryResult.SUCCESS && result.data != null) {
                    if (step != null) {
                        step.next(result.data);
                    }

                } else {
                    Logger.e(TAG, getString(R.string.text_network_data_rec_fail));
                    Logger.e(TAG, "CConnAsyncTask error=" + result.errorStr);
                    try {
                        CDialog.showDlg(getApplicationContext(), getString(R.string.text_network_data_rec_fail), new CDialog.DismissListener() {
                            @Override
                            public void onDissmiss() {
                                finish();
                            }
                        });
                    } catch (Exception e) {

                        try {
                            CDialog.showDlg(BaseActivity.this, getString(R.string.text_network_data_rec_fail), new CDialog.DismissListener() {
                                @Override
                                public void onDissmiss() {
                                    finish();
                                }
                            });
                        } catch (Exception e2) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(BaseActivity.this);
                            builder.setMessage(getString(R.string.text_network_data_rec_fail));
                            builder.setPositiveButton(getString(R.string.text_confirm), null);
                            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    finish();
                                }
                            });
                            AlertDialog dlg = builder.create();
                            dlg.show();
                        }
                    }
                }
            }
        };

        CConnAsyncTask asyncTask = new CConnAsyncTask();
        asyncTask.execute(queryListener);
    }
}
