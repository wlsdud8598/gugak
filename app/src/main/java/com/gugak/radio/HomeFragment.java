package com.gugak.radio;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import androidx.annotation.Nullable;

import com.gugak.radio.base.BaseFragment;
import com.gugak.radio.base.IBaseFragment;

public class HomeFragment extends BaseFragment implements IBaseFragment, View.OnClickListener {

    private final String TAG = MainActivity.class.getSimpleName();

    private WebView mWebView;
    private String programUrl;

    public static BaseFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        return view;
    }


    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // XXX 백으로 데이터 보내기
        Bundle bundle = new Bundle();
        setBackData(bundle);


        mWebView = view.findViewById(R.id.webView);
//        mWebView.loadUrl("http://m.gugakfm.co.kr/gugak_mobile/radio/radio_program_main_sub.jsp?idx=RD2018003310&sub_num=787");

        Bundle b = getArguments();
        if (b != null) {
            programUrl = b.getString("PROGRAM_URL");
            mWebView.loadUrl(programUrl);
            Log.i(TAG, "Program url :" + programUrl);
        }
//
//        view.findViewById(R.id.webView).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

    }
}
