package com.gugak.radio;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.drawerlayout.widget.DrawerLayout;

import com.gugak.radio.main.MainFragment;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            replaceFragment(MainFragment.newInstance());

    }


}
