package com.gugak.radio.base;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.gugak.radio.BaseActivity;
import com.gugak.radio.MainActivity;
import com.gugak.radio.component.CDialog;
import com.gugak.radio.network.tr.ApiData;
import com.gugak.radio.network.tr.BaseData;
import com.gugak.radio.network.tr.BaseUrl;
import com.gugak.radio.network.tr.CConnAsyncTask;
import com.gugak.radio.network.tr.NetworkUtil;
import com.gugak.radio.util.Logger;

import java.io.BufferedInputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by MrsWin on 2017-02-16.
 */

public class BaseFragment extends Fragment implements IBaseFragment {
    private final String TAG = BaseFragment.class.getSimpleName();

    private final int REQUEST_PERMISSIONS_REQUEST_CODE = 33;
    private int mRequestCode = 1111;

    private IPermission mIpermission = null;

    boolean mIsLogin = false;

    private static BaseActivity mBaseActivity;
//    private CommonActionBar mActionBar;

    public static Fragment newInstance(BaseActivity activity) {
        BaseFragment fragment = new BaseFragment();
        mBaseActivity = activity;
        return fragment;
    }

    public void movePage(Fragment fragment) {
        movePage(fragment, null);
    }

    public void movePage(Fragment fragment, Bundle bundle) {
//        mActionBar = mBaseActivity.getCommonActionBar();
        mBaseActivity.replaceFragment(fragment, bundle);
    }

    public void movePageNoHistory(Fragment fragment, Bundle bundle) {
        mBaseActivity.replaceFragment(fragment, false, true ,bundle);
    }

    public void movePageRefresh(Fragment fragment, Bundle bundle) {
        mBaseActivity.replaceFragment(fragment, false, false ,bundle);
    }

    public void movePageActivity(Fragment fragment) {
        movePageActivity(fragment, null);
    }

    //activity popup추가
    public void movePagePopActivity(BaseFragment fragment) {
        movePagePopActivity(fragment, null);
    }

    public void movePageActivity(Fragment fragment, Bundle bundle) {
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity)getActivity()).movePageActivity(fragment,  bundle);
        }
    }

    public void movePageActivity(Fragment fragment, Bundle bundle, boolean anim) {
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity)getActivity()).movePageNoAnimActivity(fragment,  bundle, anim);
        }
    }

    public void movePagePopActivity(Fragment fragment, Bundle bundle) {
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity)getActivity()).movePagePopActivity(fragment,  bundle);
        }
    }

    /**
     * 사용할 레이아웃 또는 View 지정
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        onViewCreated(view, savedInstanceState);
        return view;
    }

    /**
     * 뷰가 생성된 후 세팅
     *
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onBackPressed() {
        Logger.i(TAG, "BaseFragment.onBackPressed().getActivity()=" + getActivity());
        if (getActivity() instanceof MainActivity) {
            MainActivity activity = (MainActivity) getActivity();
            activity.superBackPressed();
        } else if (getActivity() instanceof BaseActivity) {
            BaseActivity activity = (BaseActivity) getActivity();
            activity.superBackPressed();
        } else {
            mBaseActivity.onBackPressed();
        }
    }

    @Override
    public String getPageId() {
        return "PageId 입력";
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    protected void onCreateOptionsMenu(Menu menu) {
        mBaseActivity.onCreateOptionsMenu(menu);
    }

    /**
     * Back 이동시 데이터 전달
     */
    private Bundle mBackDataBundle;

    protected void setBackData(Bundle bundle) {
        mBackDataBundle = bundle;
        Intent intent = new Intent();
        intent.putExtra(BaseActivity.ACTIVITY_BACK_DATA, bundle);
         getActivity().setResult(BaseActivity.BACK_DATA_CODE, intent);
    }

    public Bundle getBackData() {
        Bundle bundle = mBackDataBundle;
        if (mBackDataBundle != null) {
//            mBackDataBundle = new Bundle(); // 초기화
            return bundle;
        } else {
            return new Bundle();
        }
    }

    public Bundle getBackData(Intent data) {
        Bundle bundle = data.getBundleExtra(BaseActivity.ACTIVITY_BACK_DATA);
//        Bundle bundle = mBackDataBundle;
        return bundle;
    }

    public void resultBackData(Bundle bundle) {
        mBackDataBundle = bundle;
        Log.i(TAG, "resultBackData="+bundle);
    }

    /**
     * 현재 보여지고 있는 Fragment
     *
     * @return
     */
    public Fragment getVisibleFragment() {
        Fragment fragment = mBaseActivity.getVisibleFragment();

        return fragment;
    }

    public void showProgress() {
        if (mBaseActivity != null)
            mBaseActivity.showProgress();
    }

    public void hideProgress() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mBaseActivity.hideProgress();
            }
        }, 1 * 500);
    }

    public void hideProgressForce() {
        mBaseActivity.hideProgressForce();
    }


    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent, null);
    }

    @Override
    public void startActivity(Intent intent, @Nullable Bundle options) {
        super.startActivity(intent, options);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode, null);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode, @Nullable Bundle options) {
        super.startActivityForResult(intent, requestCode, options);
    }

    public void startActivityForResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.i(TAG, TAG + ".onActivityResult.data="+data);
    }


    public void getData(final Context context, final BaseData cls, final Object obj, final ApiData.IStep step) {
        getData(context, cls, obj, true, step, null);
    }

    public void getData(final Context context, final BaseData cls, final Object obj, boolean isShowProgress, final ApiData.IStep step, final ApiData.IFailStep failStep) {
        if (context == null) {
            // 앱을 빠르게 취소 하고 재 시작 시 오류 발생 될 때 있음
            Logger.e(TAG , "getData context is null");
            return;
        }
        if (NetworkUtil.getConnectivityStatus(context) == false) {
            CDialog.showDlg(context, "네트워크 연결 상태를 확인해주세요.");
            return;
        }
        String url = BaseUrl.COMMON_URL;
        Logger.i(TAG, "LoadBalance.cls=" + cls + ", url=" + url);

            if (isShowProgress)
                showProgress();

        CConnAsyncTask.CConnectorListener queryListener = new CConnAsyncTask.CConnectorListener() {

            @Override
            public Object run() throws Exception {

                ApiData data = new ApiData();
                return data.getData(cls, obj);
            }

            @Override
            public void view(CConnAsyncTask.CQueryResult result) {
                hideProgress();

                if (result.result == CConnAsyncTask.CQueryResult.SUCCESS && result.data != null) {
                    if (step != null) {
                        step.next(result.data);
                    }

                } else {
                    mBaseActivity.hideProgressForce();
                    if (failStep != null) {
                        failStep.fail();
                    } else {
                        Toast.makeText(getContext(), "데이터 수신에 실패 하였습니다.", Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "CConnAsyncTask error=" + result.errorStr);
                        hideProgress();
                    }
                }
            }
        };

        CConnAsyncTask asyncTask = new CConnAsyncTask();
        asyncTask.execute(queryListener);
    }


    /**
     * 전문 외 일반 api 요청시
     * @param context
     * @param dataCls
     * @param isShowProgress
     * @param step
     * @param failStep
     */
    public void getAPIData(final Context context, final BaseData dataCls, final boolean isShowProgress, final ApiData.IStep step, final ApiData.IFailStep failStep) {
        if (context == null) {
            // 앱을 빠르게 취소 하고 재 시작 시 오류 발생 될 때 있음
            Logger.e(TAG , "getData context is null");
            return;
        }
        if (NetworkUtil.getConnectivityStatus(context) == false) {
            CDialog.showDlg(context, "네트워크 연결 상태를 확인해주세요.");
            return;
        }

//        if(!cls.getName().equals(Tr_hra_check_result_input.class.getName())) {
        if (isShowProgress)
            showProgress();
//        }

        CConnAsyncTask.CConnectorListener queryListener = new CConnAsyncTask.CConnectorListener() {

            @Override
            public Object run() throws Exception {

                ApiData data = new ApiData();
                return data.getAPIData(context, dataCls);
            }

            @Override
            public void view(CConnAsyncTask.CQueryResult result) {
                if (isShowProgress)
                    hideProgress();

                if (result.result == CConnAsyncTask.CQueryResult.SUCCESS && result.data != null) {
                    if (step != null) {
                        step.next(result.data);
                    }

                } else {
                    mBaseActivity.hideProgressForce();
                    if (failStep != null) {
                        failStep.fail();
                    } else {
                        Toast.makeText(getContext(), "데이터 수신에 실패 하였습니다.", Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "CConnAsyncTask error=" + result.errorStr);
                        hideProgress();
                    }
                }
            }
        };

        CConnAsyncTask asyncTask = new CConnAsyncTask();
        asyncTask.execute(queryListener);
    }


    /**
     * 이미지 url에서 이미지를 가져와 ImageView에 세팅한다.
     *
     * @param imgUrl
     * @param iv
     */
    public void getImageData(final String imgUrl, final ImageView iv) {
        if (imgUrl == null) {
            Logger.d(TAG, "getIndexToImageData imgUrl is null");
            return;
        }

        CConnAsyncTask.CConnectorListener queryListener = new CConnAsyncTask.CConnectorListener() {

            @Override
            public Object run() throws Exception {
                URL url = new URL(imgUrl);
                URLConnection conn = url.openConnection();
                conn.connect();
                BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());

                Bitmap bm = BitmapFactory.decodeStream(bis);
                bis.close();
                return bm;
            }

            @Override
            public void view(CConnAsyncTask.CQueryResult result) {
                if (result.result == CConnAsyncTask.CQueryResult.SUCCESS && result.data != null) {
                    Bitmap bm = (Bitmap) result.data;
                    iv.setImageBitmap(bm);
                } else {
                    Logger.e(TAG, "CConnAsyncTask error");
                }
            }
        };

        CConnAsyncTask asyncTask = new CConnAsyncTask();
        asyncTask.execute(queryListener);
    }

    public void finishStep() {
        mBaseActivity.finishStep();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public BaseFragment getFragment(Class<?> cls) {
        BaseFragment fragment = null;
        try {
            Constructor<?> co = cls.getConstructor();
            fragment = (BaseFragment) co.newInstance();
        } catch (Exception e) {
            Log.e(TAG, "getFragment", e);
        }
        return fragment;
    }

    public void reqPermissions(String[] perms, IPermission iPermission) {
        mIpermission = iPermission;
        final String[] permissions = getGrandtedPermissions(perms);
        if (permissions.length > 0) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            CDialog.showDlg(getContext(), "권한 설정 후 이용 가능합니다.", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ActivityCompat.requestPermissions(getActivity()
                            , permissions
                            , REQUEST_PERMISSIONS_REQUEST_CODE);
                }
            }, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
        } else {
            if (iPermission != null) {
                iPermission.result(true);
                mIpermission = null;
            }

        }
    }

    /**
     * 설정이 되지 않은 권한들 가져옴
     * @return
     */
    private String[] getGrandtedPermissions(String... permissions) {
        List<String> list = new ArrayList<>();
        for (String perm : permissions) {
            int isGrandted = ActivityCompat.checkSelfPermission(getContext(), perm);

            if (isGrandted != PackageManager.PERMISSION_GRANTED)
                list.add(perm);
        }

        String[] permissionArr = new String[list.size()];
        permissionArr = list.toArray(permissionArr);
        return permissionArr;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                if (mIpermission != null) {
                    mIpermission.result(true);
                    mIpermission = null;
                }
            } else if (isPermissionGransteds(grantResults)) {
                if (mIpermission != null) {
                    mIpermission.result(true);
                    mIpermission = null;
                }
            } else {
                if (mIpermission != null) {
                    mIpermission.result(false);
                    mIpermission = null;
                } else {
                    CDialog.showDlg(getContext(), "권한 설정 후 이용 가능합니다.", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            reqPermissions(permissions, mIpermission);
                        }
                    }, null);
                }
            }
        }
    }


    /**
     * 앱 재시작
     */
    public void restartMainActivity() {
        getActivity().finish();

        Intent intent = new Intent(getContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    /**
     * 권한이 정상적으로 설정 되었는지 확인
     *
     * @param grantResults
     * @return
     */
    private boolean isPermissionGransteds(int[] grantResults) {
        for (int isGranted : grantResults) {
            return isGranted == PackageManager.PERMISSION_GRANTED;
        }
        return true;
    }

    public interface IPermission {
        void result(boolean isGranted);
    }

    // 화면 회전시 초기화 방지
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
