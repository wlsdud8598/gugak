package com.gugak.radio.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.fragment.app.Fragment;


import com.gugak.radio.BaseActivity;
import com.gugak.radio.BuildConfig;
import com.gugak.radio.R;
import com.gugak.radio.util.Logger;

import java.lang.reflect.Constructor;
import java.util.Set;

/**
 * Created by mrsohn on 2017. 3. 6..
 */

public class DummyActivity extends BaseActivity {
    private static final String TAG = DummyActivity.class.getSimpleName();

    public static int reqCode = 1111;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initFragment(getFragment());
    }

    public BaseFragment getFragment() {
        Intent intent = getIntent();
        BaseFragment fragment = null;
        try {
            String className = intent.getStringExtra(FRAGMENT_NAME);
            Class<?> cl = Class.forName(className);
            Constructor<?> co = cl.getConstructor();
            fragment = (BaseFragment) co.newInstance();
        } catch (Exception e) {
            Log.e(TAG, "getFragment", e);
        }

        return fragment;
    }

    public void initFragment(BaseFragment fragment) {
        if (fragment != null) {
            int fragmentCnt = getSupportFragmentManager().getBackStackEntryCount();
            boolean isAddBackStack = fragmentCnt != 0;
            replaceFragment(fragment, isAddBackStack, false, getBundle());
        }
    }

    public static void startActivity(Activity activity, Fragment fragment, Bundle bundle) {
        Intent intent = getIntentData(activity, fragment.getClass(), bundle);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.fragment_enter, R.anim.fragment_exit);
    }

    public static void startNohistoryActivity(Activity activity, Fragment fragment, Bundle bundle) {
        Intent intent = getIntentData(activity, fragment.getClass(), bundle);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.fragment_enter, R.anim.fragment_exit);
    }

    public static void startActivity(Activity activity, Fragment fragment, Bundle bundle, boolean anim) {
        Intent intent = getIntentData(activity, fragment.getClass(), bundle);
        activity.startActivity(intent);
        if(anim)
            activity.overridePendingTransition(R.anim.fragment_enter, R.anim.fragment_exit);
        else
            activity.overridePendingTransition(0, 0);
    }

    public static void startActivity(Activity activity, Class<?> cls, Bundle bundle) {
        Intent intent = getIntentData(activity, cls, bundle);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.fragment_enter, R.anim.fragment_exit);
    }

    public static void startActivityForResult(Activity activity, int reqCode, Class<?> cls, Bundle bundle, boolean isAnim) {
        Intent intent = getIntentData(activity, cls, bundle);
        activity.startActivityForResult(intent, reqCode, null);
        if (isAnim)
            activity.overridePendingTransition(R.anim.fragment_enter, R.anim.fragment_exit);
    }


    public static void startActivity(BaseFragment fragment, Class<?> cls, Bundle bundle) {
        Intent intent = getIntentData(fragment.getContext(), cls, bundle);
        fragment.startActivity(intent);
        fragment.getActivity().overridePendingTransition(R.anim.fragment_enter, R.anim.fragment_exit);
    }


    public static void startActivityForResult(BaseFragment fragment, int reqCode, Class<?> cls, Bundle bundle) {
        Intent intent = getIntentData(fragment.getContext(), cls, bundle);
        fragment.startActivityForResult(intent, reqCode, null);
        fragment.getActivity().overridePendingTransition(R.anim.fragment_enter, R.anim.fragment_exit);
    }

    private static Intent getIntentData(Context context, Class<?> cls, Bundle bundle) {
        Intent intent = new Intent(context, DummyActivity.class);
        intent.putExtra(FRAGMENT_NAME, cls.getName());
        intent.putExtra(FRAGMENT_BUNDLE, bundle);
        intent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        if (BuildConfig.DEBUG) {
            Logger.i(TAG, "getIntentData.intent="+intent);
            if (bundle != null) {
                Set<String> keys = bundle.keySet();
                for (String key : keys)
                    Logger.i(TAG, "getIntentData.bundle.key="+key+", "+bundle.get(key));
            }
        }
        return intent;
    }

    private Bundle getBundle() {
        Bundle bundle = getIntent().getBundleExtra(FRAGMENT_BUNDLE);
        return bundle;
    }

    @Override
    public void onBackPressed() {
        Log.i(TAG, "DummyActivity.getBackStackEntryCount="+getSupportFragmentManager().getBackStackEntryCount());
//        int fragmentCnt = getSupportFragmentManager().getBackStackEntryCount();
//        if (fragmentCnt <= 1) {
//            finish();
//        }else{
            super.onBackPressed();
//        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        BaseFragment.newInstance(this);
    }


    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, TAG+".onStop()");
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, TAG+".onDestroy()");
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.fragment_pop_enter, R.anim.fragment_pop_exit);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Logger.i(TAG, TAG + "onActivityResult.requestCode="+requestCode+", resultCode="+resultCode);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
