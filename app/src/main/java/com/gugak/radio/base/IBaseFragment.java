package com.gugak.radio.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

/**
 * Created by mrsohn on 2017. 3. 6..
 */

public interface IBaseFragment {

    View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);
    void onViewCreated(View view, @Nullable Bundle savedInstanceState);

    void onBackPressed();

    String getPageId();
}
