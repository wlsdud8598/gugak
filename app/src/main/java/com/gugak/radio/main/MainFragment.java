package com.gugak.radio.main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.gugak.radio.BaseActivity;
import com.gugak.radio.HomeFragment;
import com.gugak.radio.MainActivity;
import com.gugak.radio.R;
import com.gugak.radio.base.BaseFragment;
import com.gugak.radio.base.DummyActivity;
import com.gugak.radio.base.IBaseFragment;
import com.gugak.radio.component.CDialog;
import com.gugak.radio.network.tr.ApiData;
import com.gugak.radio.network.tr.BaseData;
import com.gugak.radio.network.tr.CConnAsyncTask;
import com.gugak.radio.network.tr.NetworkUtil;
import com.gugak.radio.network.tr.data.Tr_KA001;
import com.gugak.radio.network.tr.fingernApi.Tr_getChatLog;
import com.gugak.radio.util.Logger;
import com.gugak.radio.util.SharedPref;


/**
 * Created by MrsWin on 2017-02-16.
 */
public class MainFragment extends BaseFragment implements IBaseFragment, View.OnClickListener {
    private final String TAG = MainActivity.class.getSimpleName();
    private TextView textViewToChange1;
    private TextView textViewToChange2;
    private TextView textViewToChange3;
    private TextView textViewToChange4;
    private ImageView imgView;

    private String programUrl;

    public static BaseFragment newInstance() {
        MainFragment fragment = new MainFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment, container, false);

        return view;
    }


    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // XXX 백으로 데이터 보내기
        Bundle bundle = new Bundle();
        setBackData(bundle);

        view.findViewById(R.id.main_menu_btn1).setOnClickListener(this);
        view.findViewById(R.id.main_menu_btn2).setOnClickListener(this);
        view.findViewById(R.id.main_menu_btn3).setOnClickListener(this);

        view.findViewById(R.id.bar_homepage).setOnClickListener(this);

        imgView = (ImageView) view.findViewById(R.id.imageView);
        textViewToChange1 = (TextView) view.findViewById(R.id.bc_title);
        textViewToChange2 = (TextView) view.findViewById(R.id.player_title);
        textViewToChange3 = (TextView) view.findViewById(R.id.bc_text);
        textViewToChange4 = (TextView) view.findViewById(R.id.player_text);

        loadData("121");

    }

    private void setChildFragment(Fragment child) {
        FragmentTransaction childFt = getChildFragmentManager().beginTransaction();

        if (!child.isAdded()) {
            childFt.replace(R.id.container_layout, child);
            childFt.addToBackStack(null);
            childFt.commit();
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getChildFragmentManager().findFragmentById(R.id.container_layout);
        Logger.i(TAG, "onBackPressed.fragment=" + fragment);

        finishStep();
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.main_menu_btn1:
                loadData("121");
                break;
            case R.id.main_menu_btn2:
                loadData("122");
                break;

            case R.id.main_menu_btn3:
                loadData("381");
                break;

            case R.id.bar_homepage:
                Log.i(TAG, "onClick: bar hompage click");
//                movePage(HomeFragment.newInstance());

                //to homefragment
                Bundle bundle = new Bundle();
                bundle.putString("PROGRAM_URL", programUrl);

                movePageActivity(HomeFragment.newInstance(), bundle);

                break;

            case R.id.bar_share:
                movePageActivity(HomeFragment.newInstance());
                break;

            case R.id.bar_text:
                movePageActivity(HomeFragment.newInstance());
                break;

            case R.id.bar_msg:
                movePageActivity(HomeFragment.newInstance());
                break;
        }
    }


    public void loadData(String id) {

        Tr_KA001 tr = new Tr_KA001(id);

        getAPIData(getContext(), tr, true, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_KA001) {
                    Tr_KA001 data = (Tr_KA001) obj;

                    if (data.result.equals("yes")) {

                        textViewToChange1.setText(data.broadcast_title);
                        textViewToChange2.setText(data.broadcast_title);
                        textViewToChange3.setText(data.broadcast_text);
                        textViewToChange4.setText(data.broadcast_text);

                        if (data.img_url != null) {
                            Glide.with(getActivity()).load(data.img_url).into(imgView);
                            Log.i(TAG, "img_url : " + data.img_url);

                        }
                        if (data.prog_musiclist != null) {

                        }
                        if (data.prog_url != null) {
                            programUrl = data.prog_url;
                            int index = programUrl.indexOf("?") + 1; //"?" 다음부터
                            programUrl = "http://m.gugakfm.co.kr/gugak_mobile/radio/radio_program_main_sub.jsp?" + programUrl.substring(index);
                            Log.i(TAG, "PROGRAM URL: " + programUrl);


                        }
                        if (data.rtmp_url != null) {

                        }
                        if (data.broadcast_id != null) {

                        } else {
                            Logger.i(TAG, "KA001 : 기타오류");
                            Toast.makeText(getContext(), "실패하였습니다.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

            }
        },null);

    }

}