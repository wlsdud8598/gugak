package com.gugak.radio.network.tr;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.gugak.radio.util.JsonLogPrint;
import com.gugak.radio.util.Logger;

import org.json.JSONException;
import org.json.JSONObject;

public class ApiData {
	private final String	TAG			= ApiData.class.getSimpleName();
	public static final int	TYPTE_NONE	= -1;

	private int				trMode		= -1;
	private String Url;

    //NEW 통신 모듈
    public Object getData(BaseData baseData, Object obj) {
        JSONObject body = null;
//		IBaseData dataCls = null;

        try {
            body = baseData.makeJson(obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Url = baseData.conn_url;
        Logger.d(TAG, "baseData.conn_url="+baseData.conn_url);
//		}

//		String url = BaseUrl.COMMON_URL;

//		if (Define.getInstance().getInformation() != null) {
//			// 로드벨런싱 후 Url
//			if (TextUtils.isEmpty(Define.getInstance().getInformation().apiURL))
//				url = Define.getInstance().getInformation().apiURL;
//		}

        Logger.i(TAG, "ApiData.url="+baseData.conn_url);
        ConnectionUtil connectionUtil = new ConnectionUtil(baseData.conn_url);


        String result = null;
        if (body != null) {
//			if(Url.contains("https://app.fingern.co.kr:1334/gcross/chat/channel")){
            result = connectionUtil.doConnection(body, baseData.getClass().getSimpleName(), Url);
//			}else{
//				result = connectionUtil.doConnection(body, baseData.getClass().getSimpleName());
//			}

        }

        if (TextUtils.isEmpty(result)) {
            Logger.e(TAG, "getData.result="+result);
        } else {
            Logger.i(TAG, "####################### API RESULT."+baseData.getClass().getSimpleName()+" #####################");
            JsonLogPrint.printJson(result);
            Logger.i(TAG, "####################### API RESULT."+baseData.getClass().getSimpleName()+" #####################");
        }


        Gson gson = new Gson();
        if(result.startsWith("[")) {
            // json 배열 처리
            return baseData.gsonFromArrays(gson, result);
        }else{
            // json 단일 처리
            return gson.fromJson(result, baseData.getClass());
        }
//		Gson gson = new Gson();
//		return gson.fromJson(result, baseData.getClass());
    }

//	/**
//	 * 통신하여 json 데이터 Class<?> 세팅
//	 * @param cls
//	 * @return
//	 */
//	public Object getData(Context context, Class<?> cls, Object obj) {
//		JSONObject body = null;
//		IBaseData dataCls = null;
//		try {
//			Class<?> cl = Class.forName(cls.getName());
//			Constructor<?> co = cl.getConstructor();
//			dataCls = (BaseData) co.newInstance();
//
//			body = dataCls.makeJson(obj);
//
//		} catch (Exception e) {
//            try {
//                Class<?> cl = Class.forName(cls.getName());
//                Constructor<?> co = cl.getConstructor(Context.class);
//                dataCls = (BaseData) co.newInstance(context);
//
//                body = dataCls.makeJson(obj);
//            } catch (Exception e1) {
//                e1.printStackTrace();
//                Log.e(TAG, "ApiData Class 생성 실패", e);
//            }
//		}
//
//		if (dataCls instanceof BaseData) {
//		    BaseData baseData = (BaseData) dataCls;
//			Url = baseData.conn_url;
//		    Logger.d(TAG, "baseData.conn_url="+baseData.conn_url);
//        }
//
//        String url = BaseUrl.COMMON_URL;
//
//        if (Define.getInstance().getInformation() != null) {
//            // 로드벨런싱 후 Url
//            if (TextUtils.isEmpty(Define.getInstance().getInformation().apiURL))
//                url = Define.getInstance().getInformation().apiURL;
//        }
//
//        Logger.i(TAG, "ApiData.url="+url);
//        ConnectionUtil connectionUtil = new ConnectionUtil(url);
//
//
//		String result = null;
//		if (body != null) {
//			if(Url.contains("https://app.fingern.co.kr:1334/gcross/chat/channel")){
//				result = connectionUtil.doConnection(body, cls.getSimpleName(),Url);
//			}else{
//				result = connectionUtil.doConnection(body, cls.getSimpleName());
//			}
//
//        }
//
//		if (TextUtils.isEmpty(result)) {
//			Logger.e(TAG, "getData.result="+result);
//		} else {
//			Logger.i(TAG, "####################### API RESULT."+cls.getSimpleName()+" #####################");
//			JsonLogPrint.printJson(result);
//			Logger.i(TAG, "####################### API RESULT."+cls.getSimpleName()+" #####################");
//		}
//
//		Gson gson = new Gson();
//		return gson.fromJson(result, dataCls.getClass());
//	}




	/**
	 * 오픈 API용 커넥션 유틸
	 * @param context
	 * @return
	 */
//	public Object getAPIData(Context context, Class<? extends BaseData> cls, Object obj) {
	public Object getAPIData(Context context, BaseData dataCls) {
		String params = null;
		String url = dataCls.getConnUrl();

		Logger.i(TAG, "ApiData.url="+url);
		ConnectionAPIUtil connectionUtil = new ConnectionAPIUtil(url);

		String result ="";

        result = connectionUtil.doConnection(dataCls, params);


		if (TextUtils.isEmpty(result)) {
			Logger.e(TAG, "getData.result="+result);
		} else {
			Logger.i(TAG, "####################### API RESULT."+dataCls.getClass()+" #####################");
			JsonLogPrint.printJson(result);
			Logger.i(TAG, "####################### API RESULT."+dataCls.getClass().getName()+" #####################");
		}

		Gson gson = new Gson();
        if(result.startsWith("[")) {
            // json 배열 처리
            return dataCls.gsonFromArrays(gson, result);
		}else{
            // json 단일 처리
			return gson.fromJson(result, dataCls.getClass());
		}
	}


	public interface IStep {
		void next(Object obj);
	}

    public interface IFailStep {
        void fail();
    }
}
