package com.gugak.radio.network.tr;

import android.util.Log;


import com.gugak.radio.util.JsonLogPrint;
import com.gugak.radio.util.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class ConnectionAPIUtil {
    private final String TAG = ConnectionAPIUtil.class.getSimpleName();

    private String mUrl;
    private StringBuffer paramSb = new StringBuffer();

    public ConnectionAPIUtil() {
    }

    public ConnectionAPIUtil(String url) {
        mUrl = url;
    }


    public String getParam() {
        String params = paramSb.toString();
        if ("".equals(params.trim()) == false)
            params = params.substring(0, params.length() - 1);

        return params;
    }


    public String doConnection(BaseData tr, String params) {
        URL mURL = null;
        HttpURLConnection conn = null;
        int mIntResponse = 0;
        String result = "";
        try {
            mURL = new URL(tr.getConnUrl());

            conn = (HttpURLConnection) mURL.openConnection();
            conn.setConnectTimeout(3 * 1000);
            conn.setReadTimeout(3 * 1000);
            conn.setRequestMethod("GET");
            conn.setUseCaches(false);
            conn.setDoOutput(true);
//            conn.setRequestProperty("Accept-Charset", "EUC-KR");

            Log.i(TAG, "###############  ConnectionAPIUtil." + tr.getClass().getName() + "  ###############");
            Log.i(TAG, "url=" + mURL);


            OutputStream os = conn.getOutputStream();
//            os.write("json=".getBytes("EUC-KR"));
//            os.write(body.toString().getBytes("EUC-KR"));
            if (params != null) {
                params = URLEncoder.encode(params, "UTF-8");
                os.write(params.getBytes());

                JsonLogPrint.printJson(params.toString());
                Logger.i(TAG, "encordingParams="+params);
            } else {
                Logger.e(TAG, "Param is Null");
            }

            Log.i(TAG, "###############  ConnectionAPIUtil." + tr.getClass().getName() + "  ###############");
            os.flush();
            os.close();
            mIntResponse = conn.getResponseCode();
        } catch (Exception e) {
            e.printStackTrace();
            mIntResponse = 1000;
        }

        if (mIntResponse == HttpURLConnection.HTTP_OK) {
            BufferedReader br;
            try {
                br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                for (; ; ) {
                    String line = null;
                    try {
                        line = br.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (line == null) break;
                    result += line;
                }
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
        }
        // 불필요 부분 제거
//        result = result.replace("<?xml version=\"1.0\" encoding=\"utf-8\"?><string xmlns=\"http://tempuri.org/\">", "").replace("</string>", "");
        Logger.d(TAG, "doAPiConnection.result::"+tr.getClass().getName()+"::=" + result);
        Logger.d(TAG, "\n\n\n");
        return result;
    }



    public String doConnection(BaseData tr, String params, String fingerNUrl) {
        URL mURL = null;
        StringBuilder sb = new StringBuilder();
        HttpURLConnection urlConnection = null;
        try {
            mURL = new URL(fingerNUrl);


            urlConnection = (HttpURLConnection) mURL.openConnection();
//            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("GET");
            urlConnection.setUseCaches(false);
            urlConnection.setConnectTimeout(50000);
            urlConnection.setReadTimeout(50000);
            urlConnection.connect();
            //You Can also Create JSONObject here
//            OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream());
////            out.write(params);// here i sent the parameter
//            out.close();
            int HttpResult = urlConnection.getResponseCode();
            Log.i(TAG,  "HttpResult="+HttpResult);
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        urlConnection.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                Log.e("new Test", "" + sb.toString());
                return sb.toString();
            } else {
                Log.e(" ", "" + urlConnection.getResponseMessage());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
//        } catch (JSONException e) {
//            e.printStackTrace();
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return null;
    }

//    public static synchronized String encodeURI(String s) {
//        String[] findList = {"#", "+", "&", "%", " "};
//        String[] replList = {"%23", "%2B", "%26", "%25", "%20"};
//        return StringUtils.replaceEach(s, findList, replList);
//    }


}
