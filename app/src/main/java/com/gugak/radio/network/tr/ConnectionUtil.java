package com.gugak.radio.network.tr;

import android.text.TextUtils;
import android.util.Log;

import com.gugak.radio.util.JsonLogPrint;
import com.gugak.radio.util.Logger;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;


public class ConnectionUtil {
    private final String TAG = ConnectionUtil.class.getSimpleName();

    private String mUrl;
    private StringBuffer paramSb = new StringBuffer();

    public ConnectionUtil() {
    }

    public ConnectionUtil(String url) {
        mUrl = url;
    }


    public String getParam() {
        String params = paramSb.toString();
        if ("".equals(params.trim()) == false)
            params = params.substring(0, params.length() - 1);

        return params;
    }


    public String doConnection(JSONObject body, String tr) {
        URL mURL;
        HttpURLConnection conn = null;
        int mIntResponse = 0;
        String result = "";
        try {

            mURL = new URL(BaseUrl.COMMON_URL);

            conn = (HttpURLConnection) mURL.openConnection();
            conn.setConnectTimeout(3 * 1000);
            conn.setReadTimeout(3 * 1000);
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setRequestProperty("Accept-Charset", "EUC-KR");

            Log.i(TAG, "###############  ConnectionUtil." + tr + "  ###############");
            Log.i(TAG, "url=" + mURL);


            OutputStream os = conn.getOutputStream();
            os.write("json=".getBytes("EUC-KR"));
//            os.write(body.toString().getBytes("EUC-KR"));
            if (body != null) {
                String params = URLEncoder.encode(body.toString(), "EUC-KR");
                os.write(params.getBytes());

                JsonLogPrint.printJson(body.toString());
                Logger.i(TAG, "encordingParams="+params);
            } else {
                Logger.e(TAG, "Param is Null");
            }

            Log.i(TAG, "###############  ConnectionUtil." + tr + "  ###############");
            os.flush();
            os.close();
            mIntResponse = conn.getResponseCode();
        } catch (Exception e) {
            Log.d("hsh", "getJSONData ex" + e);
            mIntResponse = 1000;
        }

        if (mIntResponse == HttpURLConnection.HTTP_OK) {
            BufferedReader br;
            try {
                br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                for (; ; ) {
                    String line = null;
                    try {
                        line = br.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (line == null) break;
                    result += line;
                }
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
        }
        // 불필요 부분 제거
        result = result.replace("<?xml version=\"1.0\" encoding=\"utf-8\"?><string xmlns=\"http://tempuri.org/\">", "").replace("</string>", "");
        Logger.i(TAG, "doConnection.result=" + result);
        return result;
    }



    //NEW 통신 모듈
    public String doConnection(JSONObject body, String tr,String Url) {
        URL mURL;
        StringBuilder sb = new StringBuilder();
        HttpURLConnection urlConnection = null;
        int mIntResponse = 0;
        String result = "";
        try {

            URL url = new URL(Url);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.setUseCaches(false);
            urlConnection.setConnectTimeout(5 * 1000);
            urlConnection.setReadTimeout(5 * 1000);
            urlConnection.setRequestProperty("Accept-Charset", "EUC-KR");
            urlConnection.connect();
            //You Can also Create JSONObject here
            OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream());
            if (body != null) {
                String params = URLEncoder.encode("strJson="+body.toString(), "EUC-KR");
                out.write("strJson="+body.toString());

                JsonLogPrint.printJson(body.toString());
                Logger.i(TAG, "encordingParams="+params);
            } else {
                Logger.e(TAG, "Param is Null");
            }
            out.close();
            int HttpResult = urlConnection.getResponseCode();
            Log.i(TAG,  "HttpResult="+HttpResult);
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        urlConnection.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                result = sb.toString().replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<string xmlns=\"http://tempuri.org/\">", "").replace("</string>", "");

                Log.e("new Test", "" + result);
                return result;
            } else {
                Log.e(" ", "" + urlConnection.getResponseMessage());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
//        } catch (JSONException e) {
//            e.printStackTrace();
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return null;
    }

//    public static synchronized String encodeURI(String s) {
//        String[] findList = {"#", "+", "&", "%", " "};
//        String[] replList = {"%23", "%2B", "%26", "%25", "%20"};
//        return StringUtils.replaceEach(s, findList, replList);
//    }

}
