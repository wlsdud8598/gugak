package com.gugak.radio.network.tr.data;

import com.google.gson.annotations.SerializedName;
import com.gugak.radio.network.tr.BaseData;
import com.gugak.radio.network.tr.BaseUrl;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 *
 FUNCTION NAME 알리미 리스트

 strJson={"img_url":"http:\/\/www.gugakfm.co.kr\/DataFiles\/Radio\/PROG\/bts[2].png",
 "broadcast_title":"바투의 상사디야","result":"yes","prog_musiclist":
 "http:\/\/www.gugakfm.co.kr\/gugak_web\/radio\/radio_program_board.jsp?sub_id=499&idx=RD2016002476",
 "prog_url":"http:\/\/www.gugakfm.co.kr\/gugak_web\/radio\/radio_program_main_sub.jsp?idx=RD2016002476&sub_num=787",
 "broadcast_text":"월~금 | 14:00 ~15:55","rtmp_url":
 "rtmp:\/\/mgugaklive.nowcdn.co.kr\/gugakradio\/gugakradio.stream","broadcast_id":"RD2016002476"}

 Output
 변수명		설명
 "img_url":"",
 "broadcast_title":"",
 "result":"",
 "prog_musiclist":"",
 "prog_url":"",
 "broadcast_text":"",
 "rtmp_url":"",
 "broadcast_id":""

 */

public class Tr_KA001 extends BaseData {
	private final String TAG = Tr_KA001.class.getSimpleName();


	public static class RequestData {

    }

	public Tr_KA001() {
//		mContext = context;
		super.conn_url = "http://m.gugakfm.co.kr/gugak_ddk_2/radioOnair.jsp?broadcast_type=121";
	}

    public Tr_KA001(String ChannelId) {
        super.conn_url = "http://m.gugakfm.co.kr/gugak_ddk_2/radioOnair.jsp?broadcast_type="+ChannelId;
    }

	@Override
	public JSONObject makeJson(Object obj) throws JSONException {
		if (obj instanceof RequestData) {
            JSONObject body = new JSONObject();

            RequestData data = (RequestData) obj;

//            body.put("img_url", data.img_url);
//            body.put("broadcast_title", data.broadcast_title);
//            body.put("result", data.result);
//            body.put("prog_musiclist", data.prog_musiclist);
//            body.put("prog_url", data.prog_url);
//            body.put("broadcast_text", data.broadcast_text);
//            body.put("rtmp_url", data.rtmp_url);
//            body.put("broadcast_id", data.broadcast_id);

			return body;
		}

		return super.makeJson(obj);
	}

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

    @SerializedName("img_url")
    public String img_url;
    @SerializedName("broadcast_title")
    public String broadcast_title;
    @SerializedName("result")
    public String result;
    @SerializedName("prog_musiclist")
    public String prog_musiclist;
    @SerializedName("prog_url")
    public String prog_url;
    @SerializedName("broadcast_text")
    public String broadcast_text;
    @SerializedName("rtmp_url")
    public String rtmp_url;
    @SerializedName("broadcast_id")
    public String broadcast_id;

}
