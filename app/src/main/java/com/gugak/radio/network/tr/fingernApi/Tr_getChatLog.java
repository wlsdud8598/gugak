package com.gugak.radio.network.tr.fingernApi;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.gugak.radio.network.tr.BaseData;

import java.util.ArrayList;
import java.util.List;


public class Tr_getChatLog extends BaseData {
    private final String TAG = Tr_getChatLog.class.getSimpleName();

    public Tr_getChatLog() {

    }

    public static class RequestData {

//        public String mber_sn;
//        public String misson_typ;
//        public String pageNumber;

    }

    public Tr_getChatLog(String ChannelId) {
        super.conn_url = "http://m.gugakfm.co.kr/gugak_ddk_2/radioOnair.jsp?broadcast_type="+ChannelId;
    }


    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

    @SerializedName("img_url")
    public String img_url;
    @SerializedName("broadcast_title")
    public String broadcast_title;
    @SerializedName("result")
    public String result;
    @SerializedName("prog_musiclist")
    public String prog_musiclist;
    @SerializedName("prog_url")
    public String prog_url;
    @SerializedName("broadcast_text")
    public String broadcast_text;
    @SerializedName("rtmp_url")
    public String rtmp_url;
    @SerializedName("broadcast_id")
    public String broadcast_id;


    @Override
    public List<? extends BaseData> gsonFromArrays(Gson gson, String json) {
        List<Tr_getChatLog> list = gson.fromJson(json, new TypeToken<List<Tr_getChatLog>>(){}.getType());
        return list;
    }
}
