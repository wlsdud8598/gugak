package com.gugak.radio.network.tr.fingernApi;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.gugak.radio.network.tr.BaseData;

import java.util.ArrayList;
import java.util.List;


public class Tr_responseChat extends BaseData {
    private final String TAG = Tr_responseChat.class.getSimpleName();

    public enum Sender {
        TALK_SENDER_BOT(0) , TALK_SENDER_ME(1);

        private final int type;
        Sender(int type) {
            this.type = type;
        }

        public int type() {
            return type;
        }
    }

    public Tr_responseChat(Sender senter) {
        sender = senter;
    }

    public static class RequestData {

//        public String mber_sn;
//        public String misson_typ;
//        public String pageNumber;

    }

//    public Tr_responseChat(String Url) {
//        super.conn_url = Url;
//    }
    public Tr_responseChat(String Url) {
        super.conn_url = Url;
    }



    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/
        @SerializedName("channelId")
        public String channelId;
        @SerializedName("chatId")
        public String chatId;
        @SerializedName("chatSeqNo")
        public String chatSeqNo;

        @SerializedName("sender")   // BOT, ME;
        private Sender sender;// = new ArrayList<>();

        @SerializedName("payload")
        public Payload payload;

//        @SerializedName("payload")
        public static class Payload  {
            @SerializedName("cbTalkNo")
            public String cbTalkNo;
            @SerializedName("cbTalkLayout")
            public String cbTalkLayout;
            @SerializedName("cbMsg")
            public String cbMsg;
            @SerializedName("cbDate")
            public String cbDate;
            @SerializedName("cbAct")
            public String cbAct;
            @SerializedName("cbActVal")
            public String cbActVal;
            @SerializedName("cbBtns")
            public List<cbBtns> cbBtns_list = new ArrayList<>(); //

        // 혈당리턴
            @SerializedName("actType")
            public String actType;
            @SerializedName("bloodSugarResult")
            public String bloodSugarResult;
            @SerializedName("bloodSugarSituation")
            public String bloodSugarSituation;

        //버튼리턴
            @SerializedName("cbBtnNo")
            public String cbBtnNo;
            @SerializedName("cbBtnOdr")
            public String cbBtnOdr;
            @SerializedName("cbNxtSubjNo")
            public String cbNxtSubjNo;
            @SerializedName("cbNxtTalkNo")
            public String cbNxtTalkNo;
            @SerializedName("cbBtnMsg")
            public String cbBtnMsg;
            @SerializedName("cbBtnAct")
            public String cbBtnAct;
            @SerializedName("cbBtnActVal")
            public String cbBtnActVal;
        }

    class cbBtns {
        @SerializedName("cbBtnNo") //
        public String cbBtnNo;
        @SerializedName("cbBtnOdr") //
        public String cbBtnOdr;
        @SerializedName("cbNxtSubjNo") //
        public String cbNxtSubjNo;
        @SerializedName("cbNxtTalkNo") //
        public String cbNxtTalkNo;
        @SerializedName("cbBtnMsg") //
        public String cbBtnMsg;
        @SerializedName("cbBtnAct") //
        public String cbBtnAct;
        @SerializedName("cbBtnActVal") //
        public String cbBtnActVal;
    }

        @SerializedName("project")
        public String project;
        @SerializedName("createdAt")
        public String createdAt;
        @SerializedName("updatedAt")
        public String updatedAt;


    @Override
    public List<? extends BaseData> gsonFromArrays(Gson gson, String json) {
        List<Tr_responseChat> list = gson.fromJson(json, new TypeToken<List<Tr_responseChat>>(){}.getType());
        return list;
    }

    /**
     * CareBot인지 자신인지 여부
     * @return
     */
    public Sender getSender() {
        return sender;
    }

}
