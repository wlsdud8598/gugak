package com.gugak.radio.network.tr.fingernApi;

import com.google.gson.annotations.SerializedName;
import com.gugak.radio.network.tr.BaseData;
import com.gugak.radio.util.Logger;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 핑거앤 채널 생성
 */

public class Tr_sendChat extends BaseData {
    private final String TAG = Tr_sendChat.class.getSimpleName();

    public static class RequestData extends Tr_sendChat {
        public String chatId;
        public String actType = "bloodsugar";
        public String bloodSugarResult;
        public String bloodSugarSituation;
        public String createdAt;


        public RequestData(String ChannelId) {
            super(ChannelId);
        }
    }

    public static class BtnRequestData  {
        public String chatId;
        public String cbBtnNo;
        public String cbBtnOdr;
        public String cbNxtSubjNo;
        public String cbNxtTalkNo;
        public String cbBtnMsg;
        public String cbBtnAct;
        public String cbBtnActVal;
        public String createdAt;

    }

    public Tr_sendChat(String ChannelId) {
        super.conn_url = "https://app.fingern.co.kr:1334/gcross/chat/channel/" + ChannelId + "/chatItem";
    }




    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        Logger.i(TAG, "makeJson.obj=" + obj);
        if (obj instanceof RequestData) {
            JSONObject body = new JSONObject();
            JSONObject body2 = new JSONObject();
            RequestData data = (RequestData) obj;

            body.put("chatId", data.chatId );
            body2.put("actType",data.actType);
            body2.put("bloodSugarResult",data.bloodSugarResult);
            body2.put("bloodSugarSituation",data.bloodSugarSituation);
            body.put("payload", body2 );
            body.put("createdAt",data.createdAt);

            return body;
        }

        else if (obj instanceof BtnRequestData) {
            JSONObject body = new JSONObject();
            JSONObject body2 = new JSONObject();
            BtnRequestData data = (BtnRequestData) obj;

            body.put("chatId", data.chatId );
            body2.put("cbBtnNo",data.cbBtnNo);
            body2.put("cbBtnOdr",data.cbBtnOdr);
            body2.put("cbNxtSubjNo",data.cbNxtSubjNo);
            body2.put("cbNxtTalkNo",data.cbNxtTalkNo);
            body2.put("cbBtnMsg",data.cbBtnMsg);
            body2.put("cbBtnAct",data.cbBtnAct);
            body2.put("cbBtnActVal",data.cbBtnActVal);
            body.put("payload", body2 );
            body.put("createdAt",data.createdAt);

            return body;
        }

        return super.makeJson(obj);
    }

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

    @SerializedName("channelId")
    public String channelId; //


}
