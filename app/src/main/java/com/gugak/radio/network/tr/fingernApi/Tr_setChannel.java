package com.gugak.radio.network.tr.fingernApi;

import com.google.gson.annotations.SerializedName;
import com.gugak.radio.network.tr.BaseData;
import com.gugak.radio.util.Logger;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 핑거앤 채널 생성
 */

public class Tr_setChannel extends BaseData {
    private final String TAG = Tr_setChannel.class.getSimpleName();

    public static class RequestData {


    }


    public Tr_setChannel() {
        super.conn_url = "https://app.fingern.co.kr:1334/gcross/chat/channel";
    }

    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        Logger.i(TAG, "makeJson.obj=" + obj);
        if (obj instanceof RequestData) {
            JSONObject body = new JSONObject();
            RequestData data = (RequestData) obj;

            body.put("userClass", "gcrossClient" );

            return body;
        }

        return super.makeJson(obj);
    }

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

    @SerializedName("userClass")
    public String userClass; //
    @SerializedName("channelId")
    public String channelId; //

}
