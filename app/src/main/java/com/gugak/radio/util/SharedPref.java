package com.gugak.radio.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

public class SharedPref {
    private final String TAG = SharedPref.class.getSimpleName();
	
	private static SharedPref instance;
	private static Context mContext;
	
	public final String PREF_NAME = "greencross";

    public static String IS_STEP_SAVE_INFO = "is_step_save_info";         // 걸음 저장여부
    public static String SAVED_LOGIN_ID = "saved_login_id";         // 저장된 아이디
    public static String SAVED_LOGIN_PWD = "saved_login_pwd";       // 저장된 아이디
    public static String IS_LOGIN_SUCEESS = "is_login_suceess";     // 로그인 성공 여부
    public static String IS_AUTO_LOGIN = "is_auto_login";           // 자동 로그인 여부

    public static String MENU_INDEX1 = "MENU_INDEX1";           // 메인화면 메뉴선택 저장

    public static String TOKEN_ID = "TOKEN_ID";           // 토큰아이디

    public static String SERVICE_GUIDE = "SERVICE_GUIDE";           // 서비스가이드 체크

    public static String SPLASH_PERMISSION_CHECK = "splash_permission_check";     // 기본 음식 데이터

    public static String PUSH_MOVE_INDEX = "PUSH_MOVE_INDEX"; // 푸시 이동 저장

    public static String PSY_CHECK_DATA = "PSY_CHECK_DATA"; // 심리데이터
    public static String PSY_CHECK_DATA_CODE = "PSY_CHECK_DATA_CODE"; // 심리코드
    public static String PSY_CHECK_DATA_TITLE = "PSY_CHECK_DATA_TITLE"; // 심리타이틀


    public static String WOMANCARE_CHECK_DATA = "WOMANCARE_CHECK_DATA"; // 우먼케어데이터
    public static String WOMANCARE_CHECK_DATA_CODE = "WOMANCARE_CHECK_DATA_CODE"; // 우먼케어코드
    public static String WOMANCARE_CHECK_DATA_TITLE = "WOMANCARE_CHECK_DATA_TITLE"; // 우먼케어타이틀

    public static String LUCKYBOX_MISSION_CLEAR = "LUCKYBOX_MISSION_CLEAR";
    public static String LUCKYBOX_MISSION_NUMBER = "LUCKYBOX_MISSION_NUMBER";

    public static String LUCKYBOX_NEW_COUNT = "LUCKYBOX_NEW_CHECK"; //럭키박스 New
    public static String ALARM_NEW_CHECK = "ALARM_NEW_CHECK"; //알리미 msg New
    public static String ALARM_NEW_INDEX = "ALARM_NEW_INDEX"; //알리미 N 표시 및 idx값
    public static String ALARM_GUBUN = "ALARM_GUBUN"; //알리미 구분

    public static String USER_GENDER = "USER_GENDER"; //사용자 성별
    public static String USER_AGE = "USER_AGE"; //사용자 나이

    public static String INTRO_DUMMY_POP = "intro_dummypop";     // 인트로 소개 더미팝업

    public static String LINK_SERVICE_DATA = "LINK_SERVICE_DATA";
    public static String LINK_CONTENT_DATA1 = "LINK_CONTENT_DATA1";
    public static String LINK_CONTENT_DATA2 = "LINK_CONTENT_DATA2";

    public static String AUTHORIZATION_CHECK_NEW = "AUTHORIZATION_CHECK_NEW"; //상담/예약 인증여부
    public static String DIRECT_CALL = "DIRECT_CALL"; //인증 성공후 전화연결
    public static String APPAGREE_CHECK = "APPAGREE_CHECK"; //앱이용약관 동의 여부
    public static String EXP_AGREE_RESULT = "EXP_AGREE_RESULT";

    public static String COACH_START_DATE = "COACH_START_DATE"; //건강코칭 시작 날짜
    public static String COACH_SUCCES = "COACH_SUCCES"; //건강코칭 성공
    public static String COACH_FAIL = "COACH_FAIL"; //건강코칭 실패
    public static String COACH_SUCCES_W1 = "COACH_SUCCES_W1"; //건강코칭 주차별 성공 여부
    public static String COACH_SUCCES_W2 = "COACH_SUCCES_W2"; //건강코칭 주차별 성공 여부
    public static String COACH_SUCCES_W3 = "COACH_SUCCES_W3"; //건강코칭 주차별 성공 여부
    public static String COACH_SUCCES_W4 = "COACH_SUCCES_W4"; //건강코칭 주차별 성공 여부
    public static String COACH_SUCCES_W5 = "COACH_SUCCES_W5"; //건강코칭 주차별 성공 여부

    public static String IS_CHATBOT_FIRST = "IS_CHATBOT_FIRST"; // 챗봇 진입했었는지 여부
    public static String CHATBOT_INFO_AGE = "CHATBOT_INFO_AGE"; // 추천상품 나이
    public static String CHATBOT_INFO_SEX = "CHATBOT_INFO_SEX"; // 추천상품 성별
    public static String CHATBOT_INFO_SON = "CHATBOT_INFO_SON"; // 추천상품 자녀수

    public static String SEND_LOG_DATE = "SEND_LOG_DATE"; // 로그 데이터 보낸 날짜

    public static String NOTI_TAB_SAVE = "NOTI_TAB_SAVE"; //노티 탭 저장



    public static SharedPref getInstance() {
		if (instance == null) {
			instance = new SharedPref();
		}
		return instance;
	}

	public void initContext(Context context) {
        mContext = context;
    }

	// 값 불러오기
    public String getPreferences(String key){
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return pref.getString(key, "");
    }

 // 값 불러오기
    public int getPreferences(String key, int defValue){
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return pref.getInt(key, defValue);
    }

    // 값 불러오기
    public float getPreferences(String key, float defValue){
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return pref.getFloat(key, defValue);
    }
 // 값 불러오기
    public boolean getPreferences(String key, boolean defValue){
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return pref.getBoolean(key, defValue);
    }

    // 값 불러오기
    public ArrayList<String> getPreferences(String key, ArrayList<String>  defValue){
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        String JSONList = new Gson().toJson(defValue);
        ArrayList<String> Result = new Gson().fromJson(pref.getString(key, JSONList), new TypeToken<ArrayList<String>>() {
        }.getType());
        return Result;
    }





    // 값 저장하기
    public void savePreferences(String key, String val){
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, val);
        editor.commit();
    }

 // 값 저장하기
    public void savePreferences(String key, int val){
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(key, val);
        editor.commit();
    }

    // 값 저장하기
    public void savePreferences(String key, float val){
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putFloat(key, val);
        editor.commit();
    }

    // 값 저장하기
    public void savePreferences(String key, boolean val){
        if (mContext == null) {
            Logger.e(TAG, "mContext is null");
        } else {
            SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(key, val);
            editor.commit();
        }
    }

    // 값 저장하기
    public void savePreferences(String key, ArrayList<String> val){
        if (mContext == null) {
            Logger.e(TAG, "mContext is null");
        } else {
            SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            String JSONList = new Gson().toJson(val);
            editor.putString(key, JSONList);
            editor.commit();
        }
    }


    // 값(Key Data) 삭제하기
    public void removePreferences(String key){
        if (mContext == null) {
            Logger.e(TAG, "mContext is null");
        } else {
            SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.remove(key);
            editor.commit();
        }
    }

    // 값(ALL Data) 삭제하기
    public void removeAllPreferences(){
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();
    }
}
