package com.gugak.radio.util.cameraUtil;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import androidx.core.content.FileProvider;


import com.gugak.radio.BuildConfig;
import com.gugak.radio.R;
import com.gugak.radio.util.Logger;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by hyochan on 2016. 9. 30..
 */
public class ProviderUtil {
    private static final String TAG = ProviderUtil.class.getSimpleName();
    public static final String PROVIDER_AUTHORITIES = "com.greencross.sugar.fileprovider";
    public static String IMAGE_SAVE_PATH = Environment.getExternalStorageDirectory().getPath()+ File.separator+"GreenCare"+File.separator+ MediaStore.MEDIA_IGNORE_FILENAME;
    public static String IMAGE_SAVE_PATH1 = Environment.getExternalStorageDirectory().getPath()+ File.separator+"KyoboService";




//    private static File imageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
    private static File imageDirectory = new File(IMAGE_SAVE_PATH);
    private static File imageDirectory1 = new File(IMAGE_SAVE_PATH1);
    private static String imageFileName = "";
    private static String outputPath = "";

    public static File getOutputFile(Uri uri) {
        return getOutputFile(new File(uri.getPath()).getName());
    }

    public static File getOutputFile(String imageFileName) {
        return new File(imageDirectory, imageFileName);
    }

    public static File getOutputFile1(String imageFileName) {
        return new File(imageDirectory1, imageFileName);
    }

    public static String getOutputFilePath(Uri uri) {
        return getOutputFile(uri).getAbsolutePath();
    }

    public static String getOutputFilePath(String imageFileName) {
        return getOutputFile(imageFileName).getAbsolutePath();
    }

    public static Uri getOutputMediaFileUri(Context context, File file) {
        Log.d(TAG, "file:" + file);
        Log.d(TAG, "BuildConfig.APPLICATION_ID:" + BuildConfig.APPLICATION_ID);
        return FileProvider.getUriForFile(context, context.getString(R.string.file_provider), file);

    }

//    public static Uri getOutputMediaFileUri(Context context) {
//        try {
//            return getOutputMediaFileUri(context, getOutputMediaFile());
//        } catch (IOException e) {
//            Log.d(TAG, e.getLocalizedMessage());
//            e.printStackTrace();
//            return null;
//        }
//    }

    public static File getOutputMediaFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        imageFileName = timeStamp;
//        File storageDir = new File(imageDirectory, "GreenCare");
//        File noMediaFile = new File(storageDir.getPath() + File.separator + MediaStore.MEDIA_IGNORE_FILENAME);
        File noMediaFile = new File(IMAGE_SAVE_PATH);

        if (imageDirectory.exists() == false) {
            imageDirectory.mkdirs();
            Logger.i(TAG, "Create Storage Directory ::: "+imageDirectory.getPath());
        }

        if (noMediaFile.exists() == false) {
            noMediaFile.createNewFile();
            Logger.i(TAG, "Create Ignore file Create ::: "+MediaStore.MEDIA_IGNORE_FILENAME);
        }

        File file = File.createTempFile(
                imageFileName,          /* prefix */
                ".jpg",             /* suffix */
                imageDirectory              /* directory */
        );

        outputPath = "file:" + file.getAbsolutePath();
        return file;
    }


    public static File getOutputMediaFile(String fileName) throws IOException {
        imageFileName = fileName;
        if (imageDirectory.exists() == false)
            imageDirectory.mkdirs();

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".mp4",         /* suffix */
                imageDirectory      /* directory */
        );

        outputPath = "file:" + image.getAbsolutePath();
        return image;
    }


}
